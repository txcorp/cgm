#!/bin/bash
# Usage: sh FindOCEConfigFile FileName Dir1 Dir2 Dir3 ...
# Return: Full path to FileName if found. Else return NOTFOUND.
#
filename=$1
retdata="NOTFOUND"
shift 1
for dirname in "$@"
do
  if [ -d $dirname ]; then
    #echo "find $dirname -type f -name $filename -print -quit"
    diroutput_log="`find $dirname -type f -name $filename -print -quit`"
    nout="`echo $diroutput_log | grep $filename | wc -l`"
    #echo $diroutput_log
    #echo $nout
    if [ "$nout" -gt "0" ]; then
      retdata="$diroutput_log"
    fi
  fi
done
echo $retdata
