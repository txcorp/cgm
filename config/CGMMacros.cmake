#
# Collection of helper macros for CMake configuration of CGM
#

function (cgm_add_library name isinterface)
  add_library("${name}" ${ARGN})
  if (${isinterface})
    set_target_properties("${name}"
      PROPERTIES
        ARCHIVE_OUTPUT_DIRECTORY "${CGM_BINARY_DIR}/lib"
        LIBRARY_OUTPUT_DIRECTORY "${CGM_BINARY_DIR}/lib"
        RUNTIME_OUTPUT_DIRECTORY "${CGM_BINARY_DIR}/bin")
  endif(${isinterface})
  install(
    TARGETS   "${name}"
    EXPORT    cgm
    RUNTIME   DESTINATION bin
    LIBRARY   DESTINATION lib
    ARCHIVE   DESTINATION lib
    COMPONENT Runtime)
  set_property(GLOBAL APPEND
    PROPERTY cgm_libs
    "${name}")
  set_property(GLOBAL APPEND
    PROPERTY cgm_export_targets
    "${name}")
  get_target_property(target_type
    "${name}" TYPE)
  if (BUILD_SHARED_LIBS AND target_type STREQUAL "STATIC_LIBRARY")
    set_target_properties("${name}"
      PROPERTIES
        POSITION_INDEPENDENT_CODE TRUE)
  endif ()
endfunction ()

function (cgm_source_interface var)
  set(all)

  foreach (src IN LISTS ${ARGN})
    if (IS_ABSOLUTE "${src}")
      list(APPEND all
        "${src}")
    else ()
      list(APPEND all
        "${CMAKE_CURRENT_SOURCE_DIR}/${src}")
    endif ()
  endforeach ()

  set(${var} "${all}" PARENT_SCOPE)
endfunction ()

function (cgm_install_headers)
  install(
    FILES       ${ARGN}
    DESTINATION include/cgm
    COMPONENT   Development)
endfunction ()

function (cgm_target_link_libraries name)
  target_link_libraries("${name}" ${ARGN})
  set_property(GLOBAL APPEND
    PROPERTY cgm_deplibs
    ${ARGN})
endfunction ()


