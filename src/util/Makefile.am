# The name of the library to build
noinst_LTLIBRARIES = 
if BUILD_CGM
  noinst_LTLIBRARIES += libcgm_util.la
endif

SUBDIRS = cgm

# The directory where headers will be installed
libcgm_util_la_includedir = $(includedir)
AM_CPPFLAGS += -I$(top_builddir)/src/util/cgm -I@abs_top_srcdir@/src/util/cgm 

# The non-template sources
# If any of these do not need to be installed, move them
# to the _SOURCES list above.
libcgm_util_la_SOURCES = \
  AllocMemManagersList.cpp \
  AppUtil.cpp \
  ArrayBasedContainer.cpp \
  CCubitFile.cpp \
  CGMFileOptions.cpp \
  CommandFeedback.hpp \
  CpuTimer.cpp \
  Cubit2DPoint.cpp \
  CubitBox.cpp \
  CubitColor.cpp \
  CubitCoordinateSystem.cpp \
  CubitDirIterator.cpp \
  CubitDynamicLoader.cpp \
  CubitEntity.cpp \
  CubitEvent.cpp \
  CubitEventDispatcher.cpp \
  CubitFile.cpp \
  CubitFileFEModel.cpp \
  CubitFileIOWrapper.cpp \
  CubitFileMetaData.cpp \
  CubitFileSimModel.cpp \
  CubitFileUtil.cpp \
  CubitMatrix.cpp \
  CubitMessage.cpp \
  CubitMessageHandler.cpp \
  CubitObservable.cpp \
  CubitObserver.cpp \
  CubitOperationEvent.cpp \
  CubitPlane.cpp \
  CubitSparseMatrix.cpp \
  CubitString.cpp \
  CubitTransformMatrix.cpp \
  CubitUndo.cpp \
  CubitUtil.cpp \
  CubitVector.cpp \
  DLList.cpp \
  GMem.cpp \
  GetLongOpt.cpp \
  GfxDebug.cpp \
  IntersectionTool.cpp \
  MemoryBlock.cpp \
  MemoryManager.cpp \
  ParamCubitPlane.cpp \
  ProgOptions.cpp \
  PlanarParamTool.cpp \
  RandomMersenne.cpp \
  SDLList.cpp \
  SettingHandler.cpp \
  SettingHolder.cpp \
  StubProgressTool.cpp \
  TDUPtr.cpp \
  TextProgressTool.cpp \
  ToolData.cpp \
  ToolDataUser.cpp \
  TtyProgressTool.cpp

# Headers to be installed.  If any file in this list should
# not be installed, move it to the _SOURCES list above.
nobase_libcgm_util_la_include_HEADERS = \
  cgm/AbstractTree.hpp \
  cgm/AppUtil.hpp \
  cgm/ArrayBasedContainer.hpp \
  cgm/CCubitFile.hpp \
  cgm/CastTo.hpp \
  cgm/CGMFileOptions.hpp \
  cgm/CpuTimer.hpp \
  cgm/Cubit2DPoint.hpp \
  cgm/CubitBox.hpp \
  cgm/CubitBoxStruct.h \
  cgm/CubitColorConstants.hpp \
  cgm/CubitCoordEvent.hpp \
  cgm/CubitCoordinateSystem.hpp \
  cgm/CubitDefines.h \
  cgm/CubitDirIterator.hpp \
  cgm/CubitDynamicLoader.hpp \
  cgm/CubitEntity.hpp \
  cgm/CubitEvent.hpp \
  cgm/CubitEventDispatcher.hpp \
  cgm/CubitFile.hpp \
  cgm/CubitFileFEModel.hpp \
  cgm/CubitFileIOWrapper.hpp \
  cgm/CubitFileMetaData.hpp \
  cgm/CubitFileSimModel.hpp \
  cgm/CubitLoops.hpp \
  cgm/CubitUndo.hpp \
  cgm/CubitFileUtil.hpp \
  cgm/CubitInputFile.hpp \
  cgm/CubitMatrix.hpp \
  cgm/CubitMessage.hpp \
  cgm/CubitObservable.hpp \
  cgm/CubitObserver.hpp \
  cgm/CubitOperationEvent.hpp \
  cgm/CubitPlane.hpp \
  cgm/CubitPlaneStruct.h \
  cgm/CubitSparseMatrix.hpp \
  cgm/CubitString.hpp \
  cgm/CubitTransformMatrix.hpp \
  cgm/CubitUtil.hpp \
  cgm/CubitVector.hpp \
  cgm/CubitVectorStruct.h \
  cgm/DLIList.hpp \
  cgm/DLList.hpp \
  cgm/DrawingToolDefines.h \
  cgm/DynamicDLIIterator.hpp \
  cgm/DynamicTreeIterator.hpp \
  cgm/ElementType.h \
  cgm/FacetShapeDefs.hpp \
  cgm/GMem.hpp \
  cgm/GeometryDefines.h \
  cgm/GetLongOpt.hpp \
  cgm/GfxDebug.hpp \
  cgm/IGUIObservers.hpp \
  cgm/IndexedDouble.hpp \
  cgm/IntersectionTool.hpp \
  cgm/InvalidEntity.hpp \
  cgm/KDDTree.hpp \
  cgm/KDDTreeNode.hpp \
  cgm/LocalStart.h \
  cgm/ManagedPtrVector.hpp \
  cgm/MemoryBlock.hpp \
  cgm/MemoryManager.hpp \
  cgm/OctTree.hpp \
  cgm/OctTreeCell.hpp \
  cgm/ParamCubitPlane.hpp \
  cgm/ParamTool.hpp \
  cgm/PlanarParamTool.hpp \
  cgm/PriorityQueue.hpp \
  cgm/ProgOptions.hpp \
  cgm/ProgressTool.hpp \
  cgm/RStarTree.hpp \
  cgm/RStarTreeNode.hpp \
  cgm/RTree.hpp \
  cgm/RTreeNode.hpp \
  cgm/RandomMersenne.hpp \
  cgm/SDLCAMergePartnerList.hpp \
  cgm/SDLList.hpp \
  cgm/SettingHandler.hpp \
  cgm/SettingHolder.hpp \
  cgm/StubProgressTool.hpp \
  cgm/TDCellIndex.hpp \
  cgm/TDUPtr.hpp \
  cgm/TDVector.hpp \
  cgm/TextProgressTool.hpp \
  cgm/ToolData.hpp \
  cgm/ToolDataUser.hpp \
  cgm/TtyProgressTool.hpp \
  cgm/WeightedOctree.hpp \
  cgm/CubitMessageHandler.hpp \
  cgm/CubitColor.hpp 
# If template defs are included, then the template definitions
# need to be installed with the headers.  Otherwise they need
# to be compiled.
if INCLUDE_TEMPLATE_DEFS
  nobase_libcgm_util_la_include_HEADERS += \
      cgm/KDDTree.cpp \
      cgm/KDDTreeNode.cpp \
      cgm/OctTreeCell.cpp \
      cgm/OctTree.cpp \
      cgm/PriorityQueue.cpp \
      cgm/RTree.cpp \
      cgm/RTreeNode.cpp \
      cgm/RStarTree.cpp \
      cgm/RStarTreeNode.cpp \
      cgm/WeightedOctree.cpp
else
  libcgm_util_la_SOURCES += \
      cgm/KDDTree.cpp \
      cgm/KDDTreeNode.cpp \
      cgm/OctTreeCell.cpp \
      cgm/OctTree.cpp \
      cgm/PriorityQueue.cpp \
      cgm/RTree.cpp \
      cgm/RTreeNode.cpp \
      cgm/RStarTree.cpp \
      cgm/RStarTreeNode.cpp \
      cgm/WeightedOctree.cpp
endif

libcgm_util_la_LIBADD = ${CGM_EXT_LDFLAGS} -ldl

nobase_includedir = $(includedir)/cgm
nodist_nobase_include_HEADERS = cgm/CGMUtilConfigure.h

EXTRA_DIST = CMakeLists.txt

DISTCLEANFILES = cgm/CGMUtilConfigure.h
