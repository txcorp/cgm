project(cgm_occ)

SET(occ_srcs
  OCCAttribSet.cpp
  OCCBody.cpp
  OCCCoEdge.cpp
  OCCCurve.cpp
  OCCDrawTool.cpp
  OCCHistory.cpp
  OCCLoop.cpp
  OCCLump.cpp
  OCCModifyEngine.cpp
  OCCPoint.cpp
  OCCQueryEngine.cpp
  OCCShapeAttributeSet.cpp
  OCCShell.cpp
  OCCSurface.cpp)

set(occ_headers
  cgm/OCCAttribSet.hpp
  cgm/OCCBody.hpp
  cgm/OCCCoEdge.hpp
  cgm/OCCCurve.hpp
  cgm/OCCDrawTool.hpp
  cgm/OCCGeometryCreator.hpp
  cgm/OCCLoop.hpp
  cgm/OCCLump.hpp
  cgm/OCCModifyEngine.hpp
  cgm/OCCPoint.hpp
  cgm/OCCQueryEngine.hpp
  cgm/OCCShapeAttributeSet.hpp
  cgm/OCCShell.hpp
  cgm/OCCSurface.hpp)

if (WIN32)
  add_definitions(
    -DWNT
    -DHAVE_OCC_DEF
    -DOCC_INC_FLAG)
endif ()

# build cubit occ library from sources
cgm_add_library(cgm_occ 1
    ${occ_srcs} ${occ_headers})

#message("Compile and Link OCC sources with ${OpenCascade_LIBRARIES}")
target_link_libraries(cgm_occ
    cgm_geom
    cgm_util
    ${OpenCascade_LIBRARIES})

include_directories(
    ${OCE_INCLUDE_DIRS}
    ${CMAKE_SOURCE_DIR}/src/geom/cgm
    ${CMAKE_BINARY_DIR}/src/geom/cgm
    ${CMAKE_SOURCE_DIR}/src/util/cgm
    ${CMAKE_BINARY_DIR}/src/util/cgm
    ${CMAKE_CURRENT_SOURCE_DIR}/cgm
    )
target_compile_definitions(cgm_occ
  PUBLIC
   ${CGMA_OCC_DEFINES})

install(
  TARGETS   "cgm_occ"
  EXPORT    cgm_geom
  RUNTIME   DESTINATION bin
  LIBRARY   DESTINATION lib
  ARCHIVE   DESTINATION lib
  COMPONENT Runtime)

cgm_install_headers(${occ_headers})

