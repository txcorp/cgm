project(cgm_init)
#
include(CGMMacros)
#
include_directories(${OCE_INCLUDE_DIRS})

get_property(CGM_DEFINES GLOBAL
  PROPERTY cgm_defines)

set(cgm_init_srcs InitCGMA.cpp)
set(cgm_init_hdrs
  cgm/InitCGMA.hpp
  # Generated files:
  "${CMAKE_CURRENT_BINARY_DIR}/cgm/CGMInitConfigure.h")

set(CGM_INIT_BUILD_SHARED_LIBS ${BUILD_SHARED_LIBS})
configure_file(
  "${CMAKE_SOURCE_DIR}/config/CGMInitConfigure.h.in"
  "${CMAKE_CURRENT_BINARY_DIR}/cgm/CGMInitConfigure.h"
  @ONLY)

include_directories(${CMAKE_CURRENT_BINARY_DIR}/cgm
    ${CMAKE_CURRENT_SOURCE_DIR}/cgm
    ${CMAKE_SOURCE_DIR}/src/util/cgm
    ${CMAKE_BINARY_DIR}/src/util/cgm
    ${CMAKE_SOURCE_DIR}/src/geom/cgm
    ${CMAKE_BINARY_DIR}/src/geom/cgm
    ${CMAKE_SOURCE_DIR}/src/geom/virtual/cgm
    ${CMAKE_SOURCE_DIR}/src/geom/facet/cgm
)

if (CGM_HAVE_OCC)
  include_directories(${CMAKE_SOURCE_DIR}/src/geom/OCC/cgm
  )
  link_libraries(cgm_occ)
endif (CGM_HAVE_OCC)

link_libraries(
    cgm_facet
    cgm_virtual
    cgm_geom
    cgm_util
)

set(INIT_CPPFLAGS "${CGMA_OCC_DEFINES}")
if(ENABLE_OCC)
  set(INIT_CPPFLAGS "-DHAVE_OCC ${INIT_CPPFLAGS}")
endif(ENABLE_OCC)

set(cgm_init_target cgm_init PARENT_SCOPE)
cgm_add_library(cgm_init 0 
  ${cgm_init_srcs})
  #${cgm_init_hdrs})
set(cgm_init_include_type PUBLIC)
target_compile_definitions(cgm_init
  PUBLIC
    ${CGM_DEFINES}
    ${INIT_CPPFLAGS})

set_target_properties(cgm_init PROPERTIES LINKER_LANGUAGE CXX)


cgm_install_headers(${cgm_init_hdrs})

